var assert = require("assert");
var classifier = require("classifier");
var _ = require("underscore");
var bayes;

beforeEach(function() {
  bayes = new classifier.Bayesian({
    thresholds: {
      spam: 0,
      ham: 1
    }
  });


  var ham = [
    {
      msg: "Lo-Dash gained 1,000 npm dependents in ~3 months. Sights are set on the top spot!"
    },
    {
      msg: "Doing a quick skim through HAML documentation for the app my team is building at #hn5! #hacknashville"
    }
  ];

  var spam = [
    {
      msg: "Bananas are great!"
    },
  ];


  // train ham
  _.each(ham, function(h, i) {
    bayes.train(h.msg.toLowerCase(), 'ham');
  });

  // train spam
  _.each(spam, function(h, i) {
    bayes.train(h.msg.toLowerCase(), 'spam');
  });

});

describe("Classification", function() {

  describe("Ham", function() {
    it('should return "ham" when the tweet is classified as ham', function() {
      assert.equal('ham', bayes.classify("nashville"));
      assert.equal('ham', bayes.classify("lo dash"));
      assert.equal('ham', bayes.classify("1000"));
    });
  });

  describe("Spam", function() {
    it('should return "spam" when the tweet is classified as spam', function() {
      assert.equal('spam', bayes.classify("bananas"));
    });
  });

})

/*
var test1 = bayes.classify("nashville");   // 'ham'
var test2 = bayes.classify("bananas");   // 'spam'
var test3 = bayes.classify("lo dash");   // ??
var test4 = bayes.classify("1,001");   // ??
var test4 = bayes.classify("1,001");   // ??


console.log("test1: ", test1);
console.log("test2: ", test2);
console.log("test3: ", test3);
console.log("test4: ", test4);
*/
//bayes.train("cheap replica watches", 'spam');
//bayes.train("I don't know if this works on windows", 'not');

//var category = bayes.classify("free watches");   // "spam"
//console.log(category);
